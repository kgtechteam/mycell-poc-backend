package com.turkcell.mycell.model;

import lombok.Builder;
import lombok.Data;

/**
 * Author       :   kaanalkim
 * Created on   :   2019-12-12
 **/
@Builder
@Data
public class PlanAttribute {
    private String code;
    private String name;
    private String value;
    private String subtext;
}
