package com.turkcell.mycell.model;

import com.turkcell.mycell.model.base.AbstractEntity;
import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Author       :   kaanalkim
 * Created on   :   2019-12-12
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@Document(collection = "Log")
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Log extends AbstractEntity {
    private String method;
    private String requestUri;
    private String remoteAddress;
    private String requestBody;
}
