package com.turkcell.mycell.model;

import com.turkcell.mycell.model.base.AbstractEntity;
import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Author       :   kaanalkim
 * Created on   :   2019-12-12
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@Document(collection = "Menu")
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Menu extends AbstractEntity {
    private String name;

    private List<MenuItem> submenu;
}
