package com.turkcell.mycell.model;

import com.turkcell.mycell.model.base.AbstractEntity;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.util.List;

/**
 * Author       :   kaanalkim
 * Created on   :   2019-12-12
 **/
@EqualsAndHashCode(callSuper = true)
@Builder
@Data
@Document("Plan")
public class Plan extends AbstractEntity {
    private String name;
    private BigDecimal price;
    private List<PlanAttribute> planAttributes;
}
