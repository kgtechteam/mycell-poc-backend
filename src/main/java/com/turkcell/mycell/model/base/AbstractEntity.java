package com.turkcell.mycell.model.base;

import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.util.Date;

@Data
public abstract class AbstractEntity implements Serializable {
    @Id
    private ObjectId oid;

    protected Date created;

    protected Date updated;

    protected boolean deleted = false;

    protected void onUpdate() {
        updated = new Date();
    }
}
