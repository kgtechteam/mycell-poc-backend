package com.turkcell.mycell.model;

import lombok.*;

/**
 * Author       :   kaanalkim
 * Created on   :   2019-12-12
 **/
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class MenuItem {
    private String name;
}
