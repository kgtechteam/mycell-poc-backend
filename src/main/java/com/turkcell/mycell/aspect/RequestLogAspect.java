package com.turkcell.mycell.aspect;

import com.turkcell.mycell.model.Log;
import com.turkcell.mycell.service.LogService;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

/**
 * Author       :   kaanalkim
 * Created on   :   2019-12-12
 **/
@Aspect
@Component
@Slf4j
public class RequestLogAspect {
    private LogService logService;

    @Autowired
    public RequestLogAspect(LogService logService) {
        this.logService = logService;
    }

    @Around("execution(* com.turkcell.mycell.controller..*(..)))")
    public Object log(final ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();

        Object value;

        try {
            value = proceedingJoinPoint.proceed();
        } catch (Throwable throwable) {
            throw throwable;
        } finally {
            logService.save(
                    Log.builder()
                            .method(request.getMethod())
                            .requestUri(request.getRequestURI())
                            .remoteAddress(request.getRemoteAddr())
                            .requestBody(Arrays.toString(proceedingJoinPoint.getArgs()))
                            .build()
            );
        }

        return value;
    }
}
