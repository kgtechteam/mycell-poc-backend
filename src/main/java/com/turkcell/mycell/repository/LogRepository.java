package com.turkcell.mycell.repository;

import com.turkcell.mycell.model.Log;
import com.turkcell.mycell.repository.base.BaseRepository;

/**
 * Author       :   kaanalkim
 * Created on   :   2019-12-12
 **/
public interface LogRepository extends BaseRepository<Log, String> {
}
