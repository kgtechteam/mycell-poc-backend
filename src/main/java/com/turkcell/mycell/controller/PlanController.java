package com.turkcell.mycell.controller;

import com.turkcell.mycell.controller.base.AbstractCrudController;
import com.turkcell.mycell.model.Plan;
import com.turkcell.mycell.service.PlanService;
import com.turkcell.mycell.service.base.AbstractCrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Author       :   kaanalkim
 * Created on   :   2019-12-12
 **/
@RestController
@RequestMapping("plan")
public class PlanController extends AbstractCrudController<Plan> {
    private PlanService planService;

    @Autowired
    public PlanController(PlanService planService) {
        this.planService = planService;
    }

    @Override
    protected AbstractCrudService<Plan> getService() {
        return (AbstractCrudService<Plan>) this.planService;
    }
}
