package com.turkcell.mycell.controller;

import com.turkcell.mycell.controller.base.AbstractCrudController;
import com.turkcell.mycell.model.Menu;
import com.turkcell.mycell.service.MenuService;
import com.turkcell.mycell.service.base.AbstractCrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Author       :   kaanalkim
 * Created on   :   2019-12-12
 **/
@RestController
@RequestMapping("menu")
public class MenuController extends AbstractCrudController<Menu> {
    private MenuService menuService;

    @Autowired
    public MenuController(MenuService menuService) {
        this.menuService = menuService;
    }

    @Override
    protected AbstractCrudService<Menu> getService() {
        return (AbstractCrudService<Menu>) this.menuService;
    }
}
