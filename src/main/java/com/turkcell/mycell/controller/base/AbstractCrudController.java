package com.turkcell.mycell.controller.base;

import com.turkcell.mycell.exceptions.ObjectNotFoundById;
import com.turkcell.mycell.model.base.AbstractEntity;
import com.turkcell.mycell.service.base.AbstractCrudService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;

public abstract class AbstractCrudController<T extends AbstractEntity> extends AbstractController {
    protected abstract AbstractCrudService<T> getService();

    @GetMapping("/{id}")
    public ResponseEntity<T> getById(@PathVariable("id") String oid) {
        T t = getService().get(oid);
        if (t == null) {
            logger.error("{}, {} object could not found by id: {}", getService().className, t.getClass().getName(), oid);
        }
        logger.info("{}, {} object found by id: {}, {}", getService().className, t.getClass().getName(), oid, t);
        return new ResponseEntity<T>(t, HttpStatus.OK);
    }

    @GetMapping("/all")
    public ResponseEntity<Collection<T>> getAll() {
        Collection<T> all = getService().getAll();
        logger.info("{} all object found successfully. Size: {}", getService().className, all.size());
        return ResponseEntity.ok().body(all);
    }

    @PostMapping
    public ResponseEntity<T> create(@RequestBody T t) {
        T saved = getService().save(t);
        logger.info("{}, {} saved successfully. {}", getService().className, t.getClass().getName(), saved);
        return new ResponseEntity(saved, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<T> update(@RequestBody T t) {
        T updated = getService().update(t);
        logger.info("{}, {} updated successfully. {}", getService().className, t.getClass().getName(), updated);
        return new ResponseEntity<T>(updated, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<T> deleteById(@PathVariable("id") String oid) {
        if (oid == null || getService().get(oid) == null) {
            logger.error("object could not found by id: {}", oid);
            throw new ObjectNotFoundById("object could not found by id: {}");
        } else {
            getService().delete(oid);
            logger.info("{}, Object deleted successfully by id: {}", getService().className, oid);
            return new ResponseEntity<T>(HttpStatus.OK);
        }
    }
}
