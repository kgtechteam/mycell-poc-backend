package com.turkcell.mycell;

import com.turkcell.mycell.model.Menu;
import com.turkcell.mycell.model.MenuItem;
import com.turkcell.mycell.model.Plan;
import com.turkcell.mycell.model.PlanAttribute;
import com.turkcell.mycell.service.MenuService;
import com.turkcell.mycell.service.PlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

/**
 * Author       :   kaanalkim
 * Created on   :   2019-12-12
 **/
@SpringBootApplication
public class Seeder implements CommandLineRunner {

    private MenuService menuService;
    private PlanService planService;

    @Autowired
    public Seeder(MenuService menuService, PlanService planService) {
        this.menuService = menuService;
        this.planService = planService;
    }

    @Override
    public void run(String... args) throws Exception {
        //Menu
        menuService.deleteAll();

        List<MenuItem> submenu = Arrays.asList(
                MenuItem.builder().name("HAT SİPARİŞİM").build(),
                MenuItem.builder().name("SIK SORULAN SORULAR").build(),
                MenuItem.builder().name("MYCELL ASİSTAN").build()
        );

        menuService.saveAll(
                Arrays.asList(
                        Menu.builder().name("TL Yükle").build(),
                        Menu.builder().name("Bildirimler").build(),
                        Menu.builder().name("Anasayfa").submenu(submenu).build(),
                        Menu.builder().name("Search").build(),
                        Menu.builder().name("Bize Sor").build()
                )
        );

        //Plans
        List<PlanAttribute> smallAttributes = Arrays.asList(
                PlanAttribute.builder().code("internet").value("3 GB").subtext("TO USE ON ANYTHING").build(),
                PlanAttribute.builder().code("minutes").value("500 DK").subtext("ON DIGITAL SERVICES").build(),
                PlanAttribute.builder().code("sms").value("1000").subtext("TEXTS").build(),
                PlanAttribute.builder().code("extra_minutes").value("+500 MINS").subtext("ON CALLS").build(),
                PlanAttribute.builder().code("apps").value("1").subtext("APP").build()
        );

        List<PlanAttribute> mediumAttributes = Arrays.asList(
                PlanAttribute.builder().code("internet").value("6 GB").subtext("TO USE ON ANYTHING").build(),
                PlanAttribute.builder().code("minutes").value("750 DK").subtext("ON DIGITAL SERVICES").build(),
                PlanAttribute.builder().code("sms").value("1000").subtext("TEXTS").build(),
                PlanAttribute.builder().code("extra_minutes").value("+500 MINS").subtext("ON CALLS").build(),
                PlanAttribute.builder().code("apps").value("4").subtext("APP").build()
        );

        List<PlanAttribute> largeAttributes = Arrays.asList(
                PlanAttribute.builder().code("internet").value("10 GB").subtext("TO USE ON ANYTHING").build(),
                PlanAttribute.builder().code("minutes").value("1000 DK").subtext("ON DIGITAL SERVICES").build(),
                PlanAttribute.builder().code("sms").value("1000").subtext("TEXTS").build(),
                PlanAttribute.builder().code("extra_minutes").value("+500 MINS").subtext("ON CALLS").build(),
                PlanAttribute.builder().code("apps").value("10").subtext("APP").build()
        );

        planService.saveAll(Arrays.asList(
                Plan.builder().name("MyCell Small").price(new BigDecimal(30)).planAttributes(smallAttributes).build(),
                Plan.builder().name("MyCell Medium").price(new BigDecimal(50)).planAttributes(mediumAttributes).build(),
                Plan.builder().name("MyCell Large").price(new BigDecimal(70)).planAttributes(largeAttributes).build()
        ));
    }
}