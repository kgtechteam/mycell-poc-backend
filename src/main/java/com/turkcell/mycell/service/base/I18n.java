package com.turkcell.mycell.service.base;

public interface I18n {
    String getValue(String key);

    String getValue(String key, String userLocale);

    String getValue(String key, Object[] params);

    String getValue(String key, String userLocale, Object[] params);
}