package com.turkcell.mycell.service.base;

import com.turkcell.mycell.exceptions.ObjectNotFoundById;
import com.turkcell.mycell.model.base.AbstractEntity;
import com.turkcell.mycell.repository.base.BaseRepository;
import org.springframework.beans.factory.annotation.Value;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public abstract class AbstractCrudService<T extends AbstractEntity> extends AbstractService {
    protected abstract <K extends BaseRepository<T, String>> K getRepository();

    @Value("${list.pageSize}")
    protected int pageSize;

    public T save(T t) {
        t.setUpdated(new Date());
        T saved = getRepository().save(t);
        logger.info("{} : {} saved successfully! {}", className, getName(t.getClass()), saved);
        return saved;
    }


    public Collection<T> saveAll(Collection<T> all) {
        all.forEach(t -> {
            t.setUpdated(new Date());
        });
        Collection<T> savedList = getRepository().saveAll(all);
        logger.info("{}, list saved successfully! Size: {}", className, savedList.size());
        return savedList;
    }


    public T update(T t) {
        t.setUpdated(new Date());
        T updated = getRepository().save(t);
        logger.info("{} : {} updated successfully! {}", className, getName(t.getClass()), updated);
        return updated;
    }

    public T get(String oid) {
        validateId(oid);
        Optional<T> optional = getRepository().findById(oid);
        if (!optional.isPresent()) {
            throw new ObjectNotFoundById("Object not found by ID: " + oid);
        }
        T t = optional.get();
        logger.info("{} : {} object is found by id: {}, object is: {}",
                className, optional.getClass().getName(), oid, t);
        return t;
    }

    public List<T> getAll() {
        List<T> all = getRepository().findAll();
        logger.info("{} : {} object found.", className, all.size());
        return all;
    }

    public void delete(String oid) {
        validateId(oid);
        getRepository().deleteById(oid);
        logger.info("{} : Object deleted by id {} successfully!", className, oid);
    }

    public void delete(T t) {
        getRepository().delete(t);
        logger.info("{} : {} deleted successfully!", className, getName(t.getClass()));
    }

    public void delete(T t, Boolean status) {
        t.setDeleted(status);
        getRepository().save(t);
        logger.info("{} : {} deleted successfully!", className, getName(t.getClass()));
    }

    public void deleteAll() {
        getRepository().deleteAll();
        logger.info("{}: deleted all objects", className);
    }
}
