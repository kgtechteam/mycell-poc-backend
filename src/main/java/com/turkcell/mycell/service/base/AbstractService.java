package com.turkcell.mycell.service.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractService {
    public String className = getName(getClass());
    protected Logger logger = LoggerFactory.getLogger(getClass());


    protected String getName(Class clazz) {
        String[] strings = clazz.getName().split("\\.");
        if (strings.length > 0) {
            return strings[strings.length - 1];
        }
        return "Object";
    }

    protected void validateId(Object id) {
        if (id == null || id.equals(0L)) {
            throw new IllegalArgumentException("Id may not be null or zero!");
        }
    }
}
