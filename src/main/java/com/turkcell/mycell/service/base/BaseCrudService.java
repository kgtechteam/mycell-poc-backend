package com.turkcell.mycell.service.base;

import java.util.Collection;
import java.util.List;

public interface BaseCrudService<T> {
    /**
     * Default method for save object to DB.
     *
     * @param t the object will be saved
     * @return the saved object
     */
    T save(T t);

    /**
     * Save all object in list to DB.
     *
     * @param t the list of object will be saved
     * @return the saved object list
     */
    Collection<T> saveAll(Collection<T> t);

    /**
     * Default method for update object in DB
     *
     * @param t the object will be updated
     * @return the updated object
     */
    T update(T t);

    /**
     * Find object by id.
     *
     * @param oid the id of object
     * @return the founded object
     */
    T get(String oid);

    /**
     * Find all object.
     *
     * @return the collection of objects
     */
    List<T> getAll();

    /**
     * Delete object by id.
     *
     * @param oid the id of object
     */
    void delete(String oid);

    /**
     * Delete object.
     *
     * @param t the object will be deleted
     */
    void delete(T t);

    /**
     * Delete object.
     *
     * @param t the object will be deleted
     */
    void delete(T t, Boolean idDeleted);

    /**
     * Delete all object in list.
     *
     */
    void deleteAll();
}
