package com.turkcell.mycell.service.impl;

import com.turkcell.mycell.model.Plan;
import com.turkcell.mycell.repository.PlanRepository;
import com.turkcell.mycell.service.PlanService;
import com.turkcell.mycell.service.base.AbstractCrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Author       :   kaanalkim
 * Created on   :   2019-12-12
 **/
@Service
public class PlanServiceImpl extends AbstractCrudService<Plan> implements PlanService {
    private PlanRepository planRepository;

    @Autowired
    public PlanServiceImpl(PlanRepository planRepository) {
        this.planRepository = planRepository;
    }

    @Override
    protected PlanRepository getRepository() {
        return this.planRepository;
    }
}
