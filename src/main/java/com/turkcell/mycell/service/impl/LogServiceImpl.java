package com.turkcell.mycell.service.impl;

import com.turkcell.mycell.model.Log;
import com.turkcell.mycell.repository.LogRepository;
import com.turkcell.mycell.service.LogService;
import com.turkcell.mycell.service.base.AbstractCrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Author       :   kaanalkim
 * Created on   :   2019-12-12
 **/
@Service
public class LogServiceImpl extends AbstractCrudService<Log> implements LogService {
    private LogRepository logRepository;

    @Autowired
    public LogServiceImpl(LogRepository logRepository) {
        this.logRepository = logRepository;
    }

    @Override
    protected LogRepository getRepository() {
        return this.logRepository;
    }
}
