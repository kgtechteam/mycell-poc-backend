package com.turkcell.mycell.service.impl;

import com.turkcell.mycell.model.Menu;
import com.turkcell.mycell.repository.MenuRepository;
import com.turkcell.mycell.service.MenuService;
import com.turkcell.mycell.service.base.AbstractCrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Author       :   kaanalkim
 * Created on   :   2019-12-12
 **/
@Service
public class MenuServiceImpl extends AbstractCrudService<Menu> implements MenuService {
    private MenuRepository menuRepository;

    @Autowired
    public MenuServiceImpl(MenuRepository menuRepository) {
        this.menuRepository = menuRepository;
    }

    @Override
    protected MenuRepository getRepository() {
        return this.menuRepository;
    }
}
