package com.turkcell.mycell.service;

import com.turkcell.mycell.model.Plan;
import com.turkcell.mycell.service.base.BaseCrudService;

/**
 * Author       :   kaanalkim
 * Created on   :   2019-12-12
 **/
public interface PlanService extends BaseCrudService<Plan> {
}
