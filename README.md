# KG POC MYCELL

This project is POC for mycell

## RUNNING PROJECT ON LOCAL

In comment line you can use this script for running jar. But first you run below script
```bash
mvn clean install
```

than you go to **target** folder and run below script.

```bash
java -jar mycell-0.0.1-SNAPSHOT.jar
```

## RUNNING PROJECT ON DOCKER

Download adoptopen java 11 base docker image

```bash
docker pull adoptopenjdk/openjdk11:alpine-jre

```

Build and generate the package

```bash
mvn clean install
```

generate docker image

```bash
docker build -t kg-poc/app .
```

run the docker image
```bash
docker run -it -p 8080:8080 kg-poc/app

```

## DEBUGGING PROJECT ON LOCAL

### On Eclipse

Right click on 'Application.java' and click 'debug as --> debug configuration'. Than open window find 'Java Application' right click and click 'New'.
Coming window you must set main class in project 'Application.java' and now you can start debugging your project

### On Intellij Idea

Click 'Edit Configuration' than click '+' button and search 'Spring Boot'.
Opening window you must set main you must set main class in project 'Application.java' and  now you can start debugging your project


## Service Links from local

```
Rest Swagger documentations --> http://localhost:8080/swagger-ui.html
```

## Database

in application.yaml
